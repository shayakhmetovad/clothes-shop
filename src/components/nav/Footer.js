import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => {
    return (
        <div className="footer">
            <div className="container footer-inner">
                <Link to="/home" className="footer-logo">CONCEPT</Link>
                <div className="icons">
                    <a href="https://web.whatsapp.com/" rel="noreferrer" target="_blank"><i className="fab fa-whatsapp"></i></a>
                    <a href="https://www.facebook.com/" rel="noreferrer" target="_blank"><i className="fab fa-facebook"></i></a>
                    <a href="https://www.youtube.com/" rel="noreferrer" target="_blank"><i className="fab fa-youtube"></i></a>
                </div>
                <p><a href="tel:87083807009">+7 (708) 380 - 70 - 09</a></p>
            </div>
        </div>
    )
}
export default Footer;
