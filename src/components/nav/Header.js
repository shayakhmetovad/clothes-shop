import React, {useState, useEffect} from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import store from '../../store';
import { Switch } from 'antd';
import { userInfo, changeSettings, logoutUser } from '../../store/actions/authActions';
import { filterProductsByCategory } from '../../store/actions/categoryActions';
import Modal from 'react-modal';
import { Link } from 'react-router-dom';
import { customStyles } from '../../modal/modalStyle';

const Header = () => {
    let history = useHistory()
    const [modalData, setModalData] = useState(null)
    const [modalOpen, setModalOpen] = useState(false)
    const [search, setSearch] = useState('')
    const {auth, basket} = useSelector(state => ({...state}))

    useEffect(() => {
        if(auth.isAuthenticated) {
            store.dispatch(userInfo(auth.jti))
        }
    }, [auth.isAuthenticated, auth.jti])

    const logOut = () => {
        let answer = window.confirm('Вы действительно хотите выйти?')
        if(answer) {
            store.dispatch(logoutUser(history))
        }
    }

    const login = () => {
        history.push('/')
    }

    const changeNotify = (notify) => {
        const token = window.localStorage.getItem("token")
        const data = {
            token,
            notify
        }
        store.dispatch(changeSettings(data))
    }

    const onSearch = (e) => {
        e.preventDefault()
        store.dispatch(filterProductsByCategory(search))
    }
    return (
        <header className="header">
            <div className="container header-inner">
                <div>
                    <Link to="/home" className="header-main">Главная</Link>
                    <Link  to="/basket" className="header-basket">Корзина ({basket.length})</Link>
                </div>
                <div>
                    <p className="header-center">Concept</p>
                </div>
                <div className="header-right">
                    <div className="header-input">
                        <form onSubmit={onSearch}>
                            <button><i className="fas fa-search"></i></button>
                            <input 
                                type="search"
                                name="filter" 
                                value={search} 
                                onChange={e=>setSearch(e.target.value)} 
                                placeholder="Найти вещь"
                            />
                        </form>
                    </div>
                    <div className="header-login">
                    {auth.isAuthenticated ? (
                        <span onClick={() => {setModalData({username: auth.user.username, notify: auth.user.settings.notify}); setModalOpen(true)}}>{auth.user.username}</span>
                    ) : (<span onClick={login}>Войти</span>)} 
                        <i className="far fa-user-circle"></i>
                    </div>
                </div>
            </div>

            <Modal
                isOpen={modalOpen}
                ariaHideApp={false}
                onRequestClose={()=> setModalOpen(false)}
                style={customStyles}
                contentLabel="Product Modal"
            >
                <div className="question" onClick={()=> setModalOpen(false)}>X</div>
                {modalData && (<>
                    <h3 className="title">Профиль {modalData.username}</h3>
                    <div className="space-between">
                        <p className="modal-notify">Уведомления</p>
                        {typeof auth.user === 'object' && Object.keys(auth.user).length === 0 ? (null) : (<Switch checked={auth.user?.settings?.notify} onChange={()=>changeNotify(!auth.user?.settings?.notify)}/>)}
                    </div>
                    <p className="text-danger" onClick={() => {logOut(); setModalOpen(false)}}>Выйти</p>  
                </>)}      
            </Modal>
        </header>
    )
}

export default Header;