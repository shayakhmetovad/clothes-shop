import React, {useState, useEffect} from 'react';
import { getCategories } from '../../store/actions/categoryActions';
import categoryImage from "../../media/category.png"
import { Link } from 'react-router-dom';
const Categories = () => {
    const [categories, setCategories] = useState([])

    useEffect(()=>{
        loadCategories()
    }, [])
    const loadCategories = () => 
        getCategories().then(c=> setCategories(c.data))
    return (
        <div className="app">
            <div className="container">
                <h3 className="title">Категории</h3>
                <div className="wrap">
                    {categories.map(categ => (
                        <Link to={`/home/${categ.uuid}`} key={categ.uuid} className="categ-item">
                            <div className="categ-img">
                                {/* картинки не отображаются */}
                                {/* <img src={categ.picture} alt="" /> */}
                                <img src={categoryImage} alt="Category" />
                            </div>
                            <div className="categ-info">
                                <h3>{categ.name}</h3>
                                <p>{categ.description}</p>
                            </div>
                        </Link>
                    ))}
                </div>
            </div>
        </div>
    )
}
export default Categories;
