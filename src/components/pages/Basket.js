import React, {useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import SavedProduct from './SavedProduct';
import Modal from 'react-modal';
import { REMOVE_FROM_BASKET } from '../../store/actions/types';
import { customStyles } from '../../modal/modalStyle';

const Basket = ({history}) => {
    const {basket} = useSelector((state) => ({...state}))
    const [modalOpen, setModalOpen] = useState(false)
    const dispatch = useDispatch()

    const getTotal = () => {
        return basket.reduce((currentValue, nextValue)=> {
            return currentValue + (nextValue.count * nextValue.price)
        }, 0)
    }
    
    const clearBasket = () => {
        window.localStorage.removeItem("basket")
        dispatch({
            type: REMOVE_FROM_BASKET,
            payload: [],
        })
    }
    return (
        <div className="app">
            <div className="container">
                <h3 className="title">Корзина</h3>
                <div className="basket-page">
                    <div className="basket-left">
                        {basket.map(product=>(
                            <SavedProduct key={product.uuid} product={product}/>
                        ))}
                    </div>
                    <div className="basket-right">
                        <h3>Итого</h3>
                        <span>{basket.length} вещи</span>
                        <p>Общая сумма {getTotal()} тг</p>
                        <button className="basket-btn" onClick={() => {setModalOpen(true); clearBasket()}}>Оформить</button>
                    </div>
                </div>
                <Modal
                    isOpen={modalOpen}
                    ariaHideApp={false}
                    onRequestClose={()=> setModalOpen(false)}
                    style={customStyles}
                    contentLabel="Product Modal"
                >
                    <div className="question" onClick={()=> setModalOpen(false)}>X</div>
                    <div className="basket-modal">
                        <h2>Спасибо за заказ</h2>
                        <p>Отслеживайте статус вашего заказа в профиле</p>
                        <button onClick={() => history.push('/home')} className="basket-btn">На главную</button>
                    </div>
                    
                </Modal>
            </div>
        </div>
    )
}

export default  Basket;
