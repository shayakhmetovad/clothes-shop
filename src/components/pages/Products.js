import React, {useEffect} from 'react';
import {useSelector} from 'react-redux';
import { getProductsByCategory } from '../../store/actions/categoryActions';
import store from '../../store';
import Product from './Product';

const Products = ({match}) => {
    useEffect(()=>{
        store.dispatch(getProductsByCategory(match.params.id))
    }, [match.params.id])
    const {products} = useSelector(state => ({...state}));
    const {productsList} = products;
    
    return (
        <div className="app">
            <div className="container">
                <h4 className="title">Главная</h4>
                <div className="wrap">
                    {productsList.map(product=>(
                        <Product key={product.uuid} product={product}/>
                    ))}
                    
                </div>
            </div>
        </div>
    )
}

export default Products;