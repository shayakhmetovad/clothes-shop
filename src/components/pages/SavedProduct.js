import React, {useState} from 'react';
import ProductImage from "../../media/product.png";
import Modal from 'react-modal';
import { useDispatch } from 'react-redux';
import { ADD_TO_BASKET } from '../../store/actions/types';
import { customStyles } from '../../modal/modalStyle';

const SavedProduct = ({product}) => {
    const [modalData, setModalData] = useState(null)
    const [modalOpen, setModalOpen] = useState(false)

    const dispatch = useDispatch()
    const addToBasket = () => {
        let basket = []
        if(typeof window !== "undefined") {
            if(localStorage.getItem("basket")) {
                basket = JSON.parse(localStorage.getItem("basket"));
            }
            basket.map((prod, i)=> {
                if(prod.uuid === product.uuid) {
                    basket[i].count += 1
                }
            });
            localStorage.setItem("basket", JSON.stringify(basket))

            dispatch({
                type: ADD_TO_BASKET,
                payload: basket,
            })
        }
    }
    
    const removeFromBasket = () => {
        let basket = []
        if(typeof window !== "undefined") {
            if(localStorage.getItem("basket")) {
                basket = JSON.parse(localStorage.getItem("basket"));
            }
            for(let i=0; i<basket.length; i++) {
                if(basket[i].uuid === product.uuid && basket[i].count >1) {
                    basket[i].count -= 1
                    localStorage.setItem("basket", JSON.stringify(basket))
                    dispatch({
                        type: ADD_TO_BASKET,
                        payload: basket,
                    })
                    return;
                }
                if(basket[i].uuid === product.uuid && basket[i].count <= 1) {
                    basket.splice(i, 1)
                }
            }
            localStorage.setItem("basket", JSON.stringify(basket))

            dispatch({
                type: ADD_TO_BASKET,
                payload: basket,
            })
        }
    }
    const deleteProduct = () => {
        let basket = []
        if(typeof window !== "undefined") {
            if(localStorage.getItem("basket")) {
                basket = JSON.parse(localStorage.getItem("basket"));
            }
            basket.map((prod, i)=> {
                if(prod.uuid === product.uuid) {
                    basket.splice(i,1)
                }
            });
            localStorage.setItem("basket", JSON.stringify(basket))

            dispatch({
                type: ADD_TO_BASKET,
                payload: basket,
            })
        }
    }
    
    return (
        <div className="saved-item">
            <div className="saved-img">
                <img src={ProductImage} alt="Product" />
            </div>
            <div className="saved-info">
                <div className="question remove" onClick={() => {setModalData(product.hint); setModalOpen(true)}}>?
                </div>
                <div className="question" onClick={deleteProduct}>X</div>
                <h4 className="saved-title">{product.name}</h4>
                <div className="saved-bottom">
                    <h3>Срок доставки / <span>{(product.duration/3600)/24} дня</span></h3>
                    <div className="counter saved-counter">
                        <div className="counter-btn" onClick={addToBasket}>+</div> 
                        <span>{product.count}</span>
                        <div onClick={removeFromBasket} className="counter-btn">-</div>
                    </div>
                    <h4>{product.price * product.count} тг</h4>
                </div>
            </div>
            <Modal
                isOpen={modalOpen}
                ariaHideApp={false}
                onRequestClose={()=> setModalOpen(false)}
                style={customStyles}
                contentLabel="Product Modal"
            >
                <div className="question" onClick={()=> setModalOpen(false)}>X</div>
                {modalData && (<>
                    <h3 className="modal-title">{modalData.title}</h3>
                    <p className="modal-description">{modalData.description}</p>
                </>)}
                
            </Modal>
        </div>
    )
}

export default SavedProduct;