import React, {useState, useEffect} from 'react';
import ProductImage from "../../media/product.png";
import Modal from 'react-modal';
import { useSelector, useDispatch } from 'react-redux';
import { ADD_TO_BASKET } from '../../store/actions/types';
import { customStyles } from '../../modal/modalStyle';

const Product = ({product}) => {
    const [modalData, setModalData] = useState(null)
    const [modalOpen, setModalOpen] = useState(false)
    const [count, setCount] = useState(0)
    const {basket} = useSelector((state) => ({...state})) 
    
    useEffect(() => {
        basket.map((prod, i)=>{
            if(prod.uuid === product.uuid) {
                setCount(prod.count)
            }
        })
    }, [basket, product.uuid])
    const dispatch = useDispatch()
    const addToBasket = () => {
        let basket = []
        if(typeof window !== "undefined") {
            if(localStorage.getItem("basket")) {
                basket = JSON.parse(localStorage.getItem("basket"));
            }
            for(let i=0; i<basket.length; i++) {
                if(basket[i].uuid === product.uuid) {
                    basket[i].count += 1
                    localStorage.setItem("basket", JSON.stringify(basket))
                    dispatch({
                        type: ADD_TO_BASKET,
                        payload: basket,
                    })
                    return;
                }
            }
            basket.push({
                ...product,
                count: 1
            });
            localStorage.setItem("basket", JSON.stringify(basket))
            dispatch({
                type: ADD_TO_BASKET,
                payload: basket,
            })
        }
    }
    const removeFromBasket = () => {
        let basket = []
        if(typeof window !== "undefined") {
            if(localStorage.getItem("basket")) {
                basket = JSON.parse(localStorage.getItem("basket"));
            }
            for(let i=0; i<basket.length; i++) {
                if(basket[i].uuid === product.uuid && basket[i].count >0) {
                    basket[i].count -= 1
                    localStorage.setItem("basket", JSON.stringify(basket))
                    dispatch({
                        type: ADD_TO_BASKET,
                        payload: basket,
                    })
                    return;
                }
                if(basket[i].uuid === product.uuid && basket[i].count <= 1) {
                    basket.splice(i, 1)
                }
            }
            localStorage.setItem("basket", JSON.stringify(basket))

            dispatch({
                type: ADD_TO_BASKET,
                payload: basket,
            })
        }
    }
    return (
        <div className="product-item">
            <div className="product-img">
                <img src={ProductImage} alt="Product" />
            </div>
            <div className="product-info">
                <div className="question" onClick={() => {setModalData(product.hint); setModalOpen(true)}}>?
                </div>
                <h4>{product.name}</h4>
                <h3>Срок доставки / <span>{(product.duration/3600)/24} дня</span></h3>
                <h4>{product.price} тг</h4>
                <div className="counter">
                    <div className="counter-btn" onClick={addToBasket}>+</div> 
                    <span>{count}</span>
                    {count===0 ? (null) : (<div onClick={removeFromBasket} className="counter-btn">-</div> )}
                </div>
                
            </div>
            <Modal
                isOpen={modalOpen}
                ariaHideApp={false}
                onRequestClose={()=> setModalOpen(false)}
                style={customStyles}
                contentLabel="Product Modal"
            >
                <div className="question" onClick={()=> setModalOpen(false)}>X</div>
                {modalData && (<>
                    <h3 className="modal-title">{modalData.title}</h3>
                    <p className="modal-description">{modalData.description}</p>
                </>)}
                
            </Modal>
        </div>
    )
}

export default Product;