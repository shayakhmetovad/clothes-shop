import React, {useEffect, useState} from 'react';
import { useSelector } from 'react-redux';
import { loginUser } from '../../store/actions/authActions';
import store from '../../store';

const Login = ({history}) => {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [error, setError] = useState({})

    const {auth,errors} = useSelector(state => ({...state}))

    useEffect(() => {
        if(auth.isAuthenticated) {
            history.push('/home')
        }
        setError(errors)
    }, [auth, history, errors])

    const handleSubmit = (e) => {
        e.preventDefault()
        const user = {
            email,
            password
        }
        store.dispatch(loginUser(user, history))
    }
    
    return (
        <div className="modal-window">
            <div className="modal-content">
                <h4>Войти</h4>
                <form onSubmit={handleSubmit}>
                    <input 
                        className="log-input" 
                        type="email" 
                        value={email} 
                        onChange={e=>setEmail(e.target.value)} 
                        placeholder="Логин" 
                        required
                        autoFocus
                    />
                    <input 
                        className="log-input" 
                        type="password" 
                        value={password} 
                        onChange={e=>setPassword(e.target.value)} 
                        placeholder="Пароль" 
                        required
                    />
                    {error.detail ? (<p className="login-error">Учетная запись с указанными данными не найдена</p>) : null}
                    <button 
                        type="submit" 
                        className="login-btn"
                        disabled={!email||!password}
                    >Войти</button>
                </form>
            </div>
        </div>
    )
}
export default Login;
