import axios from 'axios';
import { GET_PRODUCTS } from './types';
export const getCategories = async() => {
    return await axios.get('/products/categories/')
}
export const getProductsByCategory = (categoryID) => dispatch => {
    axios
        .get(`/products/?category=${categoryID}`)
        .then(res => {
            dispatch({
                type: GET_PRODUCTS,
                payload: res.data
            })
        })
        .catch(err => { console.log(err); });
};
export const filterProductsByCategory = (title) => dispatch => {
    axios
        .get(`/products/?search=${title}`)
        .then(res => {
            dispatch({
                type: GET_PRODUCTS,
                payload: res.data
            })
        })
        .catch(err => { console.log(err); });
};
