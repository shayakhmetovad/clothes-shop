import {SET_CURRENT_USER, GET_ERRORS, UPDATE_NOTIFY} from "./types.js";
import axios from 'axios';
import jwt_decode from 'jwt-decode';
import {setAuthToken} from "../../utils/setAuthToken";
import {message} from "antd";

export const setCurrentUser = (decoded) => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded
    }
}
export const loginUser = (userData,history) => dispatch => {
    axios.post('/token/', userData)
        .then(res => {
            const {access} = res.data
            localStorage.setItem('token', access)
            setAuthToken(access)
            const payload = jwt_decode(access)
            
            dispatch({
                type: SET_CURRENT_USER,
                payload: payload
            })
            
            message.success('Успешно авторизовались!');
            history.push('/home');
        })
        .catch(err => {
            return dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        })
}
export const userInfo = (token) => dispatch => {
    axios.get(`/users/me/`, token)
        .then(res => {
            dispatch({
                type: SET_CURRENT_USER,
                payload: res.data
            })
        })
        .catch(err => {
            return dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        })
}

export const logoutUser = (history) => dispatch => {
    localStorage.removeItem('token');
    setAuthToken(false);
    dispatch(setCurrentUser({}));
    history.push('/');
}

export const changeSettings = (data)=> dispatch => {
    axios.put(`/users/settings/`, data)
    .then(res => {
        dispatch({
            type: UPDATE_NOTIFY,
            payload: res.data
        })
        userInfo(data.token)(dispatch)
    })
    .catch(err => {}
    );
};   