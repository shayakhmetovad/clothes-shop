import { combineReducers } from "redux";
import {authReducer} from "./authReducer";
import {errorReducer} from './errorReducer';
import {categoryReducer} from "./categoryReducer";
import { basketReducer } from "./basketReducer";
export default combineReducers({
    auth: authReducer,
    errors: errorReducer,
    products: categoryReducer,
    basket: basketReducer
})