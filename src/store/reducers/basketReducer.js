import {ADD_TO_BASKET, REMOVE_FROM_BASKET} from "../actions/types";
let initialState = [];

if(typeof window !== "undefined") {
    if(localStorage.getItem("basket")) {
        initialState = JSON.parse(localStorage.getItem("basket"));
    } else {
        initialState = [];
    }
}

export const basketReducer = (state=initialState, action) => {
    switch(action.type) {
        case ADD_TO_BASKET:
            return action.payload;
        case REMOVE_FROM_BASKET:
            return action.payload;
        default:
            return state
    }
}