import axios from 'axios';
import {USER_LOGOUT,SET_CURRENT_USER,UPDATE_NOTIFY} from '../actions/types';
import isEmpty from '../../validation/isEmpty';
const initialState = {
    isAuthenticated: false,
    user: {},
    currentUserId:null
}

export const authReducer = (state=initialState, action) => {
    switch (action.type) {
        case SET_CURRENT_USER:
            return {
                ...state,
                isAuthenticated: !isEmpty(action.payload),
                user: action.payload,
                currentUserId: action.payload.id,
            }
        case USER_LOGOUT:
            localStorage.removeItem('token')
            delete axios.defaults.headers.common["Authorization"]
            return {
                ...state,
                currentUserId: null,
                
            }
        case UPDATE_NOTIFY:
            return {
                ...state,
                user: {
                    ...state.user,
                    settings: {
                        ...state.user.settings,
                        notify: action.payload.notify 
                    }
                }
            }
        default:
            return state
    }
}
