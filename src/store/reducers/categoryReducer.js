import { GET_PRODUCTS } from "../actions/types";
const initialState={
    productsList: []
};
export const categoryReducer = (state=initialState,action) => {
    switch (action.type) {
        case GET_PRODUCTS:
            return {
                ...state,
                productsList: action.payload
            }
        default:
            return state;
    }
}