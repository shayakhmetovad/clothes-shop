import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Header from './components/nav/Header';
import Login from './components/auth/Login';
import Categories from './components/pages/Categories';
import Products from './components/pages/Products';
import Basket from './components/pages/Basket';
import Footer from './components/nav/Footer';

const App = () => {
  return (
    <>
      <Header/>
      <Switch>
        <Route path="/" component={Login} exact/>
        <Route path="/home" component={Categories} exact/>
        <Route path="/home/:id" component={Products} exact/>
        <Route path="/basket" component={Basket} exact/>
      </Switch>
      <Footer/>
    </>
  );
}

export default App;
