import React, {Suspense} from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import './App.css';
import 'antd/dist/antd.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from 'react-redux';
import jwt_decode from "jwt-decode";
import {setAuthToken} from "./utils/setAuthToken";
import {setCurrentUser, logoutUser} from "./store/actions/authActions";
import store from './store';

if (localStorage.token) {
  setAuthToken(localStorage.token);
  const decoded = jwt_decode(localStorage.token);
  store.dispatch(setCurrentUser(decoded));
  const currentTime = Date.now()/1000;
  if (decoded.exp<currentTime) {
    store.dispatch(logoutUser());
    window.location.href = '/'
  }
}
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Suspense fallback={<div>Loading...</div>}>
        <Router>
          <App />
        </Router>
      </Suspense>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
